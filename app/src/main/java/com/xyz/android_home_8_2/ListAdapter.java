package com.xyz.android_home_8_2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by user on 30.01.2018.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {


    List<DataSite.BankData> list;
    Context context;


    public ListAdapter(List<DataSite.BankData> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.updateData(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_bank)
        TextView title_bank;

        @BindView(R.id.phone_bank)
        TextView phone_bank;

        @BindView(R.id.address_bank)
        TextView address_bank;

        @BindView(R.id.usd_ask)
        TextView usd_ask;

        @BindView(R.id.eur_ask)
        TextView eur_ask;

        @BindView(R.id.rub_ask)
        TextView rub_ask;

        @BindView(R.id.usd_bid)
        TextView usd_bid;

        @BindView(R.id.eur_bid)
        TextView eur_bid;

        @BindView(R.id.rub_bid)
        TextView rub_bid;

        DataSite.BankData data;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void updateData(DataSite.BankData data) {
            this.data = data;

            title_bank.setText(data.title);
            phone_bank.setText(data.phone);
            address_bank.setText(data.address);

            usd_ask.setText(data.currencies.getAskUSD());
            eur_ask.setText(data.currencies.getAskEUR());
            rub_ask.setText(data.currencies.getAskRUB());

            usd_bid.setText(data.currencies.getBidUSD());
            eur_bid.setText(data.currencies.getBidEUR());
            rub_bid.setText(data.currencies.getBidRUB());

        }

    }
}