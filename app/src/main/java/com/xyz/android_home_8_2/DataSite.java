package com.xyz.android_home_8_2;

import java.util.Arrays;


public class DataSite {
    public String sourceId;
    public String date;
    public BankData[] organizations;

    public static class BankData {
        public String title;
        public String phone;
        public String address;
        public Currency currencies;

        public static class Currency {
            public BankOperation EUR;
            public BankOperation RUB;
            public BankOperation USD;


            public static class BankOperation {
                public String ask;
                public String bid;
            }

            public String getAskRUB() {

                if (RUB != null) {
                    return "RUB ask : " + remove2symbols(RUB.ask);
                } else {
                    return "";
                }
            }

            public String getAskUSD() {
                if (USD != null) {
                    return "USD ask : " + remove2symbols(USD.ask);

                } else {
                    return "";
                }
            }

            public String getAskEUR() {
                if (EUR != null) {
                    return "EUR ask : " + remove2symbols(EUR.ask);
                } else {
                    return "";
                }
            }

            public String getBidRUB() {
                if (RUB != null) {
                    return "RUB bid : " + remove2symbols(RUB.bid);
                } else {
                    return "";
                }
            }

            public String getBidUSD() {
                if (USD != null) {
                    return "USD bid : " + remove2symbols(USD.bid);
                } else {
                    return "";
                }
            }

            public String getBidEUR() {
                if (EUR != null) {
                    return "EUR bid : " + remove2symbols(EUR.bid);
                } else {
                    return "";
                }
            }


            public String remove2symbols(String strIn) {
                //"ask": "28.5500",
                if (strIn.length()  ==6) {
                    StringBuffer stringBuffer = new StringBuffer(strIn);
                    stringBuffer.delete(5, 6);
                    return stringBuffer.toString();
                }
                if (strIn.length()  ==7) {
                    StringBuffer stringBuffer = new StringBuffer(strIn);
                    stringBuffer.delete(5, 7);
                    return stringBuffer.toString();
                }
                return strIn;
            }
        }
    }

    @Override
    public String toString() {
        return "DataSite{" +
                "sourceId='" + sourceId + '\'' +
                ", date='" + date + '\'' +
                ", organizations=" + (organizations[0].title) +
                ", kurs=" + (organizations[0].currencies.USD.ask) +
                '}';
    }


}


