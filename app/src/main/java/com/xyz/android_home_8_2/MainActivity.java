package com.xyz.android_home_8_2;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.internal.ObjectConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/*
2*. Используя http://resources.finance.ua/ru/public/currency-cash.json
(или любой другой аналог) создать приложение "Курс валют". Приложение
должно отображать текущий курс валют по трем валютам из двух-трех
банков на выбор.
 */

public class MainActivity extends AppCompatActivity {

    ListAdapter listAdapter;
    @BindView(R.id.list)
    RecyclerView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Retrofit.getBankData(new Callback<DataSite>() {
            @Override
            public void success(DataSite dataSite, Response response) {
                listAdapter = new ListAdapter(Arrays.asList(dataSite.organizations), MainActivity.this);
                LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                listView.setLayoutManager(layoutManager);
                listView.setAdapter(listAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Alert")
                        .setMessage(error.toString())
                        .setCancelable(true)
                        .show();
            }
        });


    }

    @OnClick(R.id.buttonStart)
    public void startButton() {

        Retrofit.getBankData(new Callback<DataSite>() {
            @Override
            public void success(DataSite dataSite, Response response) {


                List<DataSite.BankData> sortList = Arrays.asList(dataSite.organizations);
                Collections.sort(sortList, new Comparator<DataSite.BankData>() {
                    @Override
                    public int compare(DataSite.BankData lhs, DataSite.BankData rhs) {
                        // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending

                        if (lhs.currencies.USD != null & rhs.currencies.USD != null) {

                            int tmp = 0;
                            try {
                                tmp = Float.valueOf(lhs.currencies.USD.ask) > Float.valueOf(rhs.currencies.USD.ask) ? 1 :
                                        (Float.valueOf(lhs.currencies.USD.ask) < Float.valueOf(rhs.currencies.USD.ask) ? -1 : 0);
                            } catch (ArithmeticException e) {
                                Toast.makeText(getBaseContext(), "Sort error!", Toast.LENGTH_LONG).show();
                            }
                            return tmp;
                        }
                        return -1;
                    }
                });

                listAdapter = new ListAdapter(sortList, MainActivity.this);
                LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                listView.setLayoutManager(layoutManager);
                listView.setAdapter(listAdapter);

            }

            @Override
            public void failure(RetrofitError error) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Alert")
                        .setMessage(error.toString())
                        .setCancelable(true)
                        .show();
            }
        });

    }
}

/*
,
    {
      "id": "7oiylpmiow8iy1smaca",
      "oldId": 317,
      "orgType": 1,
      "branch": false,
      "title": "\u0410\u0441\u0432\u0438\u043e \u0411\u0430\u043d\u043a",
      "regionId": "ua,7oiylpmiow8iy1smaca",
      "cityId": "7oiylpmiow8iy1smaec",
      "phone": "0462675501",
      "address": "\u0443\u043b. \u041f\u0440\u0435\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0441\u043a\u0430\u044f, 2",
      "link": "http:\/\/organizations.finance.ua\/ru\/info\/currency\/-\/7oiylpmiow8iy1smaca\/cash",
      "currencies": {
        "EUR": {
          "ask": "35.6000",
          "bid": "34.4000"
        },
        "RUB": {
          "ask": "0.5000",
          "bid": "0.3300"
        },
        "USD": {
          "ask": "28.6000",
          "bid": "28.0000"
        }
      }
    },

 */



